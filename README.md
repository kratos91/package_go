# Ejemplo de API REST arquitectura de paquetes en golang

## Generación de documentación con go-swagger
[go-swagger] go get -u github.com/go-swagger/go-swagger/cmd/swagger@latest

```
$ GO111MODULE=on
```

### Ejecutar en la terminal
```
$ swagger generate spec -o ./docs/swagger.yaml --scan-models
```