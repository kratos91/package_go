package docs

import (
	user "bitbucket.org/kratos91/package_go/user"
)

// swagger:route POST /user Users-tag idOfUserEndpoint
// Foobar does some amazing stuff.
// responses:
//   200: userResponse

// This text will appear as description of your response body.
// swagger:response userResponse
type foobarResponseWrapper struct {
	// in:body
	Body user.UserResponse
}

// swagger:parameters idOfUserEndpoint
type userParamsWrapper struct {
	// This text will appear as description of your request body.
	// in:body
	Body user.UserRequest
}
