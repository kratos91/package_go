package app

import (
	"bitbucket.org/kratos91/package_go/app/client"
	"bitbucket.org/kratos91/package_go/user"
	"github.com/gin-gonic/gin"
)

func Start() {
	db, _ := client.Connect()
	userRepo := user.NewRepository(db)
	userService := user.NewService(userRepo)
	userHandler := user.NewManageUser(userService)

	user.Handler = userHandler

	//GIN
	router := gin.Default()
	router.GET("/user", userHandler.GetAll)
	router.POST("/user", userHandler.Create)

	router.Run()
}
