package main

import (
	"bitbucket.org/kratos91/package_go/app"
	_ "bitbucket.org/kratos91/package_go/docs"
)

func main() {
	app.Start()
}
