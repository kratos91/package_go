package user

import (
	"log"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type UserRepository interface {
	GetAll() (*[]User, error)
	Create(User) error
	Migrate()
}

type userRepositoryImpl struct {
	client *gorm.DB
}

func (r *userRepositoryImpl) GetAll() (*[]User, error) {
	var err error
	users := []User{}
	err = r.client.Debug().Model(&User{}).Limit(10).Find(&users).Error
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return &users, nil
}

func (r *userRepositoryImpl) Create(user User) error {
	var err error
	err = r.client.Debug().Create(&user).Error
	if err != nil {
		return err
	}
	return nil
}

func (r *userRepositoryImpl) Migrate() {
	err := r.client.AutoMigrate(User{})
	log.Println(err.Error())
}

func NewRepository(client *gorm.DB) UserRepository {
	return &userRepositoryImpl{
		client: client,
	}
}

func Hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

func VerifyPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}
