package user

type UserService interface {
	GetAll() (*[]User, error)
	Create(UserRequest) error
}

type userServiceImpl struct {
	repo UserRepository
}

func (s *userServiceImpl) GetAll() (*[]User, error) {
	users, err := s.repo.GetAll()
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (s *userServiceImpl) Create(reqUser UserRequest) error {
	var user User

	user.Name = reqUser.Name
	user.Password = reqUser.Password
	user.Email = reqUser.Email
	err := s.repo.Create(user)
	if err != nil {
		return err
	}
	return nil
}

func NewService(repo UserRepository) UserService {
	return &userServiceImpl{
		repo: repo,
	}
}
