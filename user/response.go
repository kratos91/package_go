package user

type UserResponse struct {
	Message string `json:"message" example:"This is a message 200"`
	Status  bool   `json:"status" example:"true"`
}
