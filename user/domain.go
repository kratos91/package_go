package user

type User struct {
	Id       int    `gorm:"size:36;primary_key" db:"id"`
	Name     string `gorm:"size:150;not null" db:"name"`
	Password string `gorm:"size:150;not null" db:"password"`
	Email    string `gorm:"size:150;not null;unique" db:"email"`
}
