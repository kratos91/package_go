package user

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

var (
	Handler ManageUser
)

type ManageUser interface {
	GetAll(*gin.Context)
	Create(*gin.Context)
}

type manageUserImpl struct {
	service UserService
}

func (h *manageUserImpl) GetAll(c *gin.Context) {
	users, err := h.service.GetAll()
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}
	c.JSON(http.StatusOK, users)
}

func (h *manageUserImpl) Create(c *gin.Context) {
	var reqUser UserRequest

	errBind := c.BindJSON(&reqUser)
	if errBind != nil {
		c.JSON(http.StatusUnprocessableEntity, errBind)
		return
	}

	err := h.service.Create(reqUser)
	if err != nil {
		c.JSON(http.StatusBadRequest, err)
		return
	}
	c.JSON(http.StatusOK, nil)
}

func NewManageUser(service UserService) ManageUser {
	return &manageUserImpl{service: service}
}
