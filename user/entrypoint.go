package user

type UserRequest struct {
	Name     string `json:"name" example:"Emiliano"`
	Password string `json:"password" example:"123456"`
	Email    string `json:"email" example:"emiliano@foo.com"`
}
